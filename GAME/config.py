import pygame
pygame.init()
pygame.display.set_caption('Paar to Karo')

# screen width and height
swidth = 600
sheight = 802
# initialising screen
scr = pygame.display.set_mode((swidth, sheight))

# loading images for boat and rock
boatImg = pygame.image.load('boat.jpg')
rockImg = pygame.image.load('rock.png')

width = 20              # width of player boat
height = 48             # height of player boat
orange = (205, 165, 0)
skblue = (135, 186, 255)
black = (0, 0, 0)

# initialized standard required variables
x = 290
y = sheight-height
vel = 6
score1 = 0
score2 = 0
c_score1 = 0
c_score2 = 0
lv1 = 1
lv2 = 1
shipvel = 0
lives_p1 = 2
lives_p2 = 2
ans = 0

# sound objects
crash_sound = pygame.mixer.Sound("crash.wav")
level_cross = pygame.mixer.Sound("level.wav")

# font rendering objects
sc_obj = pygame.font.Font(None, 36)
sc_obj2 = pygame.font.Font(None, 24)
sc_obj3 = pygame.font.Font(None, 72)
sc_obj4 = pygame.font.Font(None, 132)
clock = pygame.time.Clock()
cond = True  # running condition

# Location of all the stationary obstacles
rock_list = []
rock_list.append((50, 150))
rock_list.append((400, 150))
rock_list.append((200, 300))
rock_list.append((500, 300))
rock_list.append((100, 450))
rock_list.append((365, 450))
rock_list.append((270, 600))
rock_list.append((0, 600))
rock_list.append((520, 600))


# function to manipulate velocity as level changes
def mov_velocity(val, lv1, lv2):
    if val:
        return lv1*5
    else:
        return lv2*5


# function for crash graphics
def crash():
    crash_text = sc_obj3.render("CRASHED", 1, black)
    scr.blit(crash_text, (210, 350))
    pygame.display.update()
    pygame.mixer.Sound.play(crash_sound)
    pygame.time.delay(250)


# function for level crossed graphics
def level_c():
    lvl_crossed = sc_obj3.render("Level Crossed. Yay!", 1, black)
    scr.blit(lvl_crossed, (100, 350))
    pygame.display.update()
    pygame.mixer.Sound.play(level_cross)
    pygame.time.delay(350)
