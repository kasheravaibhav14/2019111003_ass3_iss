import pygame


# defined class for making movable obstacles
class Ship():

    def __init__(self, screen):
        self.screen = screen
        self.image = pygame.image.load('ship_obs.png')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()

    def blitme(self, x, y):  # function to set the ship
        self.rect.centerx = x
        self.rect.bottom = y
        self.screen.blit(self.image, self.rect)

    def movship(self, shipvel):  # function to move the ship
        self.rect.centerx += shipvel
        if self.rect.centerx > 640:
            self.rect.centerx = 0
