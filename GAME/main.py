import pygame
import math
import time
from config import *
from mov_obs import Ship

# Creating instances of class Ship for moving obstacles
Ship1 = Ship(scr)
Ship2 = Ship(scr)
Ship3 = Ship(scr)
Ship4 = Ship(scr)
Ship5 = Ship(scr)

ship_list = [Ship1, Ship2, Ship3, Ship4, Ship5]


# spawn the player's boat to given pos
def boat(x, y):
    scr.blit(boatImg, (x, y))


def definerectangles():
    pygame.draw.rect(scr, orange, (0, 0, 800, 13+height))
    pygame.draw.rect(scr, orange, (0, 150, 800, 13+height))
    pygame.draw.rect(scr, orange, (0, 300, 800, 13+height))
    pygame.draw.rect(scr, orange, (0, 450, 800, 13+height))
    pygame.draw.rect(scr, orange, (0, 600, 800, 13+height))
    pygame.draw.rect(scr, orange, (0, 750, 800, 13+height))


# set fixed obstacles to their position
def setRock():
    i = 0
    while i < 9:
        scr.blit(rockImg, rock_list[i])
        i = i+1


# Check collision with stationary as well as moving objects
def collCheck(x, y):
    i = 0
    while i < 9:
        a = math.sqrt(abs(rock_list[i][0]+31-x) + abs(rock_list[i][1]+8-y))
        if a < 6.0:
            return 1
        i = i+1
    i = 0
    while i < 5:
        a = ship_list[i]
        if (x > a.rect.left and x+20 < a.rect.right):
            if (y < a.rect.bottom and y > a.rect.bottom-75) or (
                    y+44 < a.rect.bottom and y+44 > a.rect.bottom-75):
                return 1
        i += 1
    return 0


# Spawn moving obstacles to initial position
def setInitShip():
    i = 0
    a = 280
    while i < 5:
        ship_list[i].rect.centerx = a
        if i % 2 == 1:
            a += 300
        else:
            a -= 170
        i += 1


# Move the moving obstacles i.e. ships
def setShip():
    Ship1.blitme(Ship1.rect.centerx, 150)
    Ship1.movship(shipvel)
    Ship2.blitme(Ship2.rect.centerx, 300)
    Ship2.movship(shipvel)
    Ship3.blitme(Ship3.rect.centerx, 450)
    Ship3.movship(shipvel)
    Ship4.blitme(Ship4.rect.centerx, 600)
    Ship4.movship(shipvel)
    Ship5.blitme(Ship5.rect.centerx, 750)
    Ship5.movship(shipvel)


# printing score, level and lives information on display
def renderScore():
    sc1 = sc_obj.render("Score1 is "+str(c_score1), 1, black)
    sc2 = sc_obj.render("Score2 is "+str(c_score2), 1, black)
    info1 = sc_obj2.render("Player 1 level is "+str(lv1), 1, black)
    info11 = sc_obj2.render("Lives remaining = "+str(lives_p1), 1, black)
    info2 = sc_obj2.render("Player 2 level is "+str(lv2), 1, black)
    info22 = sc_obj2.render("Lives remaining = "+str(lives_p2), 1, black)
    current_player = sc_obj.render("Current player is "+curr_pl, 1, black)
    scr.blit(sc2, (0, 0))
    scr.blit(sc1, (0, 770))
    scr.blit(info2, (450, 0))
    scr.blit(info22, (450, 20))
    scr.blit(info11, (450, 780))
    scr.blit(info1, (450, 760))
    scr.blit(current_player, (370, 36))


# scoring pattern as player moves up
def scoreup(y):
    sc_temp = 0
    if y < 600:
        sc_temp += 5
    if y < 600+61:
        sc_temp += 10
    if y < 450:
        sc_temp += 5
    if y < 450+61:
        sc_temp += 10
    if y < 300:
        sc_temp += 5
    if y < 300+61:
        sc_temp += 10
    if y < 150:
        sc_temp += 5
    if y < 150+61:
        sc_temp += 10
    if y < 61:
        sc_temp += 10
    return sc_temp


# scoring pattern as player moves down
def scoredown(y):
    sc_temp = 0
    if y > 600:
        sc_temp += 5
    if y > 600+61:
        sc_temp += 10
    if y > 450:
        sc_temp += 5
    if y > 450+61:
        sc_temp += 10
    if y > 300:
        sc_temp += 5
    if y > 300+61:
        sc_temp += 10
    if y > 150:
        sc_temp += 5
    if y > 150+61:
        sc_temp += 10
    if y > 61:
        sc_temp += 10
    return sc_temp


def gameScore2(y):
    global score2
    global lv2
    score2 = scoredown(y)


def gameScore1(y):
    global score1
    global lv1
    score1 = scoreup(y)


val = True  # true if player 1 is playing
setInitShip()
tim1 = time.time()  # store starting time for player 1
while cond:
    if val:
        curr_pl = "P1"
    else:
        curr_pl = "P2"
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            cond = False
    # movement keys functioning
    keys = pygame.key.get_pressed()
    # Keys for player 1
    if val:
        if keys[pygame.K_UP] and y > vel:
            y -= vel
        if keys[pygame.K_DOWN] and y + height + vel-5 < sheight:
            y += vel
        if keys[pygame.K_RIGHT] and x + width + vel < swidth:
            x += vel
        if keys[pygame.K_LEFT] and x > vel:
            x -= vel
    # keys for player 2
    else:
        if keys[pygame.K_w] and y > vel:
            y -= vel
        if keys[pygame.K_s] and y + height + vel-5 < sheight:
            y += vel
        if keys[pygame.K_d] and x + width + vel < swidth:
            x += vel
        if keys[pygame.K_a] and x > vel:
            x -= vel
    # basic setup of the game including background color,
    # obstacles and the player object itself
    scr.fill(skblue)
    definerectangles()
    setRock()
    setShip()
    boat(x, y)
    # speed of moving obstacles in various levels controlled in a
    # function mov_velocity() in config file
    shipvel = mov_velocity(val, lv1, lv2)

    # conditions for player1
    if val:
        gameScore1(y)
        if collCheck(x, y) == 1:
            lv1 = 1
            x = 290
            y = 0
            tim2 = time.time()
            c_score1 += score1-int(abs(tim2-tim1))
            val = False
            lives_p1 -= 1
            crash()
        elif y < 15:
            val = False
            x = 290
            y = 0
            tim2 = time.time()
            c_score1 += score1-int(abs(tim2-tim1))
            lv1 += 1
            level_c()

    # condition for player2
    else:
        gameScore2(y)
        if collCheck(x, y) == 1:
            lv2 = 1
            x = 290
            y = sheight-height
            tim1 = time.time()
            c_score2 += score2-int(abs(tim1-tim2))
            val = True
            lives_p2 -= 1
            crash()
        elif y > 750:
            val = True
            x = 290
            y = sheight-height
            tim1 = time.time()
            c_score2 += score2-int(abs(tim1-tim2))
            lv2 += 1
            level_c()
    renderScore()
    if lives_p1 <= 0 and lives_p2 <= 0:
        break
    pygame.display.update()
    clock.tick(45)


if c_score1 > c_score2:
    ans = 1
else:
    ans = 2


# loop to display ending stats
while(True):
    pygame.init()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pygame.quit()
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_SPACE:
                pygame.quit()
    scr.fill(skblue)
    definerectangles()
    setRock()
    setShip()
    boat(x, y)
    Winner = sc_obj3.render("Player "+str(ans)+" Wins", 1, black)
    Gover = sc_obj4.render("GAME OVER", 1, black)
    prspc = sc_obj3.render("Press Space to exit", 1, black)
    scr.blit(Gover, (40, 300))
    scr.blit(prspc, (80, 500))
    scr.blit(Winner, (140, 200))
    pygame.display.update()
