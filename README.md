# 2019111003_ASS3_ISS
ASSIGNMENT 3 ISS
CREATING GAME IN PYTHON USING PYGAME LIBRARIES


File details:

3 python files
    1.  main.py
    2.  mov_obs.py
    3.  config.py
    
2 audio files
    1.  crash.wav
    2.  level.wav
    
3 image files
    1.  boat.jpg
    2.  rock.png
    3.  ship_obs.png


The player has to cross a river with 5 partitions containing both movable and immovable obstacles.
Movable objects are made as instances of class made in mov_obs.py.
Rest all are commented in code.
